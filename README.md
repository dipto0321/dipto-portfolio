# dipto-portfolio

![Home View](media/home.jpg)

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![Travis badge](https://api.travis-ci.org/EmaSuriano/gatsby-starter-mate.svg)](https://travis-ci.org/EmaSuriano/gatsby-starter-mate)
[![eslint](https://img.shields.io/badge/eslint-enabled-green.svg)](https://eslint.org/)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![Maintainability](https://api.codeclimate.com/v1/badges/c8fc076b30bd493f0cfc/maintainability)](https://codeclimate.com/github/EmaSuriano/gatsby-starter-mate/maintainability)

> This a portfolio site. [Live demo](diptokmk47.netlify.com)



## Table of Contents

- [dipto-portfolio](#dipto-portfolio)
  - [Table of Contents](#table-of-contents)
  - [Security](#security)
  - [Background](#background)
  - [Maintainers](#maintainers)
  - [License](#license)

## Security

## Background

This portfolio is built by gatsby starter theme. [Here](https://github.com/EmaSuriano/gatsby-starter-mate) is the original project.


## Maintainers

[@dipto0321](https://github.com/dipto0321)


## License

MIT © 2019 Emanuel Suriano
