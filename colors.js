module.exports = {
  background: '#F2EDD7',
  backgroundDark: '#89ABE3',

  text: '#2D2926',

  primary: '#0063B2',
  primaryLight: '#9CC3D5',
  primaryDark: '#00539C',

  secondary: '#195190',
  secondaryLight: '#89ABE3',
  secondaryDark: '#76528B',
};
